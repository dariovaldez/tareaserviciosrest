package edu.uoc.android.restservice.ui.enter;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);


        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);

        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        //Se recibe lo que se mandó de la anterior actividad y se la guarda en una variable de tipo String
        String loginName = getIntent().getStringExtra("loginName");

        mostrarDatosBasicos(loginName);
    }

    TextView labelFollowing, labelRepositories, labelFollowers;

    private void initProgressBar() {
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);
        labelFollowing = (TextView)findViewById(R.id.labelFollowing);
        labelFollowing.setVisibility(View.INVISIBLE);
        labelRepositories = (TextView) findViewById(R.id.labelRepositories);
        labelRepositories.setVisibility(View.INVISIBLE);
        labelFollowers = (TextView) findViewById(R.id.labelFollowers);
        labelFollowers.setVisibility(View.INVISIBLE);
    }

    private void mostrarDatosBasicos(String loginName){
        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                Owner owner = response.body();
                if (owner == null) {
                    //Si no hay resultados muestra mensaje de que no existe el usuario
                    Toast.makeText(getApplicationContext(), "No hay resultados, no existe el usuario", Toast.LENGTH_SHORT).show();
                }else {
                    //Mostramos en el textView los repositorios del usuario
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    //Mostramos en el textView cuantas personas sigue el usuario
                    textViewFollowing.setText(owner.getFollowing().toString());
                    //Se muestra la imagen del usuario en el imageView, para esto usamos la libreria Picasso
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                //Estado de fallo de Retrofit, aqui se muestra un mensaje de que ocurrió un error en la petición
                Toast.makeText(getApplicationContext(), "Ha ocurrido un error en la petición", Toast.LENGTH_SHORT).show();
            }
        });


        //Llamada a la lista de seguidores del usuario
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);

        callFollowers.enqueue(new Callback<List<Followers>>() {
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                //Se crea una lista
                List<Followers> list = response.body();
                if (list == null) {
                    //Si no se encuentra el Json Array quiere decir que no se encontró al usuario
                    Toast.makeText(getApplicationContext(), "No se encontró al usuario", Toast.LENGTH_SHORT).show();
                } else {
                    //Si se encuentra el Json Array quiere decir que se encontró al usuario y se muestra su lista de seguidores
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list);
                    recyclerViewFollowers.setAdapter(adapter);

                }
            }
            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                //Estado de fallo de Retrofit, aqui se muestra un mensaje de que ocurrió un error en la petición
                Toast.makeText(getApplicationContext(), "Ha ocurrido un error en la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
