package edu.uoc.android.restservice.ui.enter;

import android.app.Application;
import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    //Se hace una lista de la clase Followers
    List<Followers> listaFollowers;
    //se crea el contexto
    Context context;

    public AdaptadorFollowers(List<Followers> listaFollowers) {
        this.listaFollowers = listaFollowers;
    }

    @Override
    public ViewHolderFollowers onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        context = parent.getContext();
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderFollowers holder, int position) {
        holder.etiNombre.setText(listaFollowers.get(position).getLogin());
        // Mostramos la imagen en el imageView creado, mostramos la imagen con la ayuda de la libreria Picasso
        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.imagen);
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView etiNombre;
        ImageView imagen;

        public ViewHolderFollowers(View itemView) {
            super(itemView);
            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);
            imagen = (ImageView) itemView.findViewById(R.id.imageViewLista);

        }
    }
}
